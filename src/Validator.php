<?php

namespace AtsHr\Helper;

/**
 * Class Validator
 */
final class Validator
{
    /**
     * Az adóazonosító számot a törvény szerint az alábbiak szerint kell képezni
     *  - az 1. számjegy konstans 8-as szám, mely az adóalany magánszemély voltára utal,
     *  - a 2–6. számjegyek a személy születési időpontja és az 1867. január 1. között eltelt napok száma,
     *  - a 7–9. számjegyek az azonos napon születettek megkülönböztetésére szolgáló véletlenszerűen képzett sorszám,
     *  - a 10. számjegy az 1–9. számjegyek felhasználásával matematikai módszerekkel képzett ellenőrző szám.
     *
     * Az adóazonosító jel tizedik számjegyét úgy kell képezni, hogy
     * az első számjegyek mindegyikét szorozni kell azzal a
     * sorszámmal, ahányadik helyet foglalja el az azonosítón belül.
     * (Első számjegy szorozva eggyel, második számjegy szorozva kettővel
     * és így tovább.)
     * Az így kapott szorzatok összegét el kell osztani 11-gyel,
     * és az osztás maradéka a tizedik számjeggyel lesz egyenlő.
     * A 7–9. számjegyek szerinti születési sorszám nem adható ki,
     * ha a 11-gyel való osztás maradéka egyenlő tízzel.
     *
     * @param      $value
     * @param null $dateOfBirth
     *
     * @return bool
     * @throws \Exception
     */
    public static function TaxId($value, $dateOfBirth = null)
    {
        if (strlen($value) != 10) {
            return false;
        }

        $valArray = str_split($value);

        if ($valArray[0] != 8) {
            return false;
        }

        $checksum = 0;
        for ($i = 0; $i < 9; $i++) {
            $item = $valArray[$i];
            $checksum += $valArray[$i] * ($i + 1);
        }

        $checksum = $checksum % 11;

        $cdv = array_pop($valArray);
        if ($checksum != $cdv) {
            return false;
        }

        if (DateHelper::isDate($dateOfBirth)) {
            if (!$dateOfBirth instanceof \DateTime) {
                $dateOfBirth = new \DateTime($dateOfBirth);
                $dateOfBirth->setTime(0,0, 0);
            }

            $startDate = new \DateTime('1867-01-01');
            $startDate->setTime(0,0, 0);

            # fck diff :P
            $days = round(abs($dateOfBirth->getTimestamp() - $startDate->getTimestamp()) / 86400,0);

            if (substr($value, 1, 5) != $days) {
                return false;
            }
        }

        return true;
    }

    /**
     * A TAJ szám első nyolc számjegyéből a
     *  - páratlan helyen állókat hárommal,
     *  - a páros helyen állókat héttel szorozzuk,
     *  - és a szorzatokat összeadjuk.
     *
     * Az összeget tízzel elosztva a maradékot
     * tekintjük a kilencedik, azaz CDV kódnak.
     *
     * @param $value
     *
     * @return bool
     */
    public static function SocialSecurityNumber($value)
    {
        if (strlen($value) == 9 && is_numeric($value)) {
            $taj_3 = $taj_7 = 0;
            for ($i = 0; $i < 8; $i++) {
                $item = $value[$i];
                if ($i % 2) {
                    $taj_7 = $taj_7 + $item;
                } else {
                    $taj_3 = $taj_3 + $item;
                }
            }

            $cdv = explode('.', round(($taj_3 * 3 + $taj_7 * 7) / 10, 1));

            if (!isset($cdv[1])) {
                $cdv[1] = 0;
            }

            if ($cdv[1] != $value[8]) {
                return false;
            }

            return true;
        }

        return false;
    }


    /** 4. §
     * (1) A bankszámlát a belföldi pénzforgalomban elsődlegesen a pénzforgalmi jelzőszám és a bankszámla elnevezése azonosítja. A pénzforgalmi jelzőszám 16 (2x8) vagy 24 (3x8) numerikus karaktert tartalmazó számsor, amelyet az alábbi szabályoknak megfelelően kell kialakítani:
     *      a) Az első nyolc karakterből (irányítókód) az első három számjegy a számlavezető hitelintézetet, a következő négy számjegy (fiókazonosító) pedig a hitelintézet fiókját vagy számlavezető helyét jelöli. A nyolcadik számjegy ellenőrzőszám.
     *      b) A 9-16. karakter vagy a 9-24. karakter a bankszámla tulajdonosának azonosító száma (ügyfélazonosító). 16 karakter hosszúságú számsor esetében a 16. számjegy, 24 karakter hosszúságú számsor esetében a 24. számjegy ellenőrzőszám. A 24 karakter hosszúságú pénzforgalmi jelzőszám 16. számjegye értelemszerűen szabadon felhasználható.
     * (2) A pénzforgalmi jelzőszám első három számjegyét (a hitelintézet azonosító kódja) a Magyar Nemzeti Bank (a továbbiakban: MNB) határozza meg, és vezeti azok nyilvántartását.
     * (3) Az ellenőrzőszámok az előttük álló számjegyek ellenőrzésére szolgálnak.
     *     Képzésüket a következő algoritmus szerint kell elvégezni: külön az 1-7., valamint külön a 9-15. vagy 9-23. számjegyeket helyértékük csökkenő sorrendjében meg kell szorozni a "9,7,3,1,...9,7,3,1" számokkal, a szorzatokat összeadjuk, és az eredmény egyes helyértékén lévő számot kivonjuk 10-ből. A különbség az ellenőrzőszám. (Ha a különbség "10", az ellenőrzőszám értéke "0".)
     * (4) A hitelintézet a pénzforgalmi jelzőszám kialakítását és belső tartalmát a fenti szabályok figyelembevételével szabadon határozza meg.
     * Forrás: CD jogtár
     *
     * @param $parts
     *
     * @return bool
     */
    public static function BankAccountNumber($parts)
    {
        $multiplier = [9, 7, 3, 1];

        if (!is_array($parts)) {
            str_replace(['-', ' '], '', $parts);

            if (in_array(strlen($parts), [16, 24])) {
                $parts = str_split($parts, 8);
            } else {
                return false;
            }
        }

        $numbersForCDV[] = $parts[0];
        $numbersForCDV[] = $parts[1].(isset($parts[2]) ? $parts[2] : '');

        foreach ($numbersForCDV as $number) {
            $sum = 0;
            $numbers = str_split($number);
            $cdv = array_pop($numbers);
            foreach ($numbers as $pos => $num) {
                $sum = $sum + ($num * $multiplier[$pos % 4]);
            }

            $checksum = 10 - $sum % 10 == 10 ? 0 : 10 - $sum % 10;

            if ($checksum != $cdv) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param     $dateOfBirth
     * @param int $yrs
     *
     * @return bool
     * @throws \Exception
     */
    public static function underAge($dateOfBirth, $yrs = 18) {
        if (DateHelper::isDate($dateOfBirth)) {
            if (!$dateOfBirth instanceof \DateTime) {
                $dateOfBirth = new \DateTime($dateOfBirth);
            }

            $underage = new \DateTime();
            $underage->sub(new \DateInterval("P{$yrs}Y"));

            if ($dateOfBirth > $underage) {
                return false;
            }
        }
        return true;
    }

    /**
     * Rfc2822 Validator
     * @param $email
     * @return bool
     */
    public static function validateRfc2822Email($email)
    {
        $regexp = '/^(?:[a-z0-9!#$%\'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+\/=?^_`{|}'.
            '~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\['.
            '\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-'.
            '9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-'.
            '9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?'.
            '|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-' .
            '\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/';

        return (bool) preg_match($regexp, $email);
    }
}
