<?php

namespace AtsHr\Helper;

use Doctrine\Common\Collections\ArrayCollection;

class MonthHelper
{
    protected $startOnWeek;
    protected $endOnWeek;
    protected $firstDay;
    protected $lastDay;

    protected $month;

    /**
     * @param $month
     * @return MonthHelper
     * @throws \Exception
     */
    public static function create($month) {
        return new self($month);
    }

    /**
     * @param $month
     * @return ArrayCollection
     * @throws \Exception
     */
    public static function days($month)
    {
        $month = self::create($month);

        $start = clone $month->getStartOnWeek();
        $end = clone $month->getEndOnWeek();

        $days = new ArrayCollection();
        do {
            $day = clone $start;
            $start->add(new \DateInterval("P1D"));
            $days->add($day);
        } while ($day->format('Y-m-d') != $end->format('Y-m-d'));

        return $days;
    }

    /**
     * MonthHelper constructor.
     * @param $month
     * @throws \Exception
     */
    public function __construct($month)
    {
        $this->month = $month;

        $this->firstDay = new \DateTime($month);
        $this->lastDay = new \DateTime($month);
        $this->lastDay->modify("last day of this month");

        $this->startOnWeek = clone $this->firstDay;
        $this->endOnWeek = clone $this->lastDay;
        $this->startOnWeek->modify("monday this week");
        $this->endOnWeek->modify("sunday this week");
    }

    /**
     * @return \DateTime
     */
    public function getStartOnWeek(): \DateTime
    {
        return $this->startOnWeek;
    }

    /**
     * @return \DateTime
     */
    public function getEndOnWeek(): \DateTime
    {
        return $this->endOnWeek;
    }

    /**
     * @return \DateTime
     */
    public function getFirstDay(): \DateTime
    {
        return $this->firstDay;
    }

    /**
     * @return \DateTime
     */
    public function getLastDay(): \DateTime
    {
        return $this->lastDay;
    }

    /**
     * @return string
     */
    public function getMonth()
    {
        return $this->month;
    }

    public function __toString()
    {
        return $this->getMonth();
    }
}
