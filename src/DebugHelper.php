<?php

namespace AtsHr\Helper;

class DebugHelper
{
    /**
     * @param array|string $data
     * @return void
     */
    public static function debugToConsole( $data )
    {

        if ( is_array( $data ) )
            $output = "<script>console.log( 'Debug Objects: " . implode( ',', $data) . "' );</script>";
        else
            $output = "<script>console.log( 'Debug Objects: " . $data . "' );</script>";

        echo $output;
    }


    /**
     * @param array|string $data
     */
    public static function commentToConsole( $data ) {

        if ( is_array( $data ) )
            $output = "/*" . implode( ',', $data) . "*/";
        else
            $output = "/*" . $data . "*/";

        echo $output;
    }
}
