<?php


namespace AtsHr\Helper;


use Melon\Contracts\Document\File;

/**
 * TODO: Move back to Aoo
 */
class DataManipulatorHelper
{
    /**
     * @param $files
     * @return array
     *
     * Result example
     * {
     *  "url": "https://127.0.0.1:8002/uploads/5ffdc3064f89d6487540eaf4",
     *  "mime": "image/png",
     *  "size": 129715,
     *  "tags": [
     *     "CV"
     *  ],
     *  "src": {
     *      "thumbnail": "https://127.0.0.1:8002/uploads/5ffdc3064f89d6487540eaf4/thumbnail",
     *      "small": "https://127.0.0.1:8002/uploads/5ffdc3064f89d6487540eaf4/small",
     *      "normal": "https://127.0.0.1:8002/uploads/5ffdc3064f89d6487540eaf4/normal",
     *      "large": "https://127.0.0.1:8002/uploads/5ffdc3064f89d6487540eaf4/large"
     *  },
     *  "download": "https://127.0.0.1:8002/uploads/5ffdc3064f89d6487540eaf4/download"
     * }
     */
    public static function fileResults($files, $baseUrl = '/'): array
    {
        return array_map(function (File $file) use ($baseUrl) {
            if (strpos($file->getMime(), 'image/') === 0) {
                return [
                    'name' => $file->getFile(),
                    'url' => $baseUrl . $file->getId(),
                    'mime' => $file->getMime(),
                    'size' => $file->getSize(),
                    'tags' => $file->getTags(),
                    'src' => [
                        'thumbnail' => $baseUrl . $file->getId() . '/thumbnail',
                        'small' => $baseUrl . $file->getId() . '/small',
                        'normal' => $baseUrl . $file->getId() . '/normal',
                        'large' => $baseUrl . $file->getId() . '/large',
                    ],
                    'download' => $baseUrl . $file->getId() . '/download',
                ];
            } else {
                return [
                    'name' => $file->getFile(),
                    'url' => $baseUrl . $file->getId(),
                    'mime' => $file->getMime(),
                    'size' => $file->getSize(),
                    'tags' => $file->getTags(),
                    'download' => $baseUrl . $file->getId() . '/download',
                    'content' => $baseUrl . $file->getId() . '/content',
                ];
            }
        }, $files);
    }
}
