<?php

namespace AtsHr\Helper;

use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;

class JMSSerializerHelper
{
    /**
     * @param $data
     * @param $groups
     * @return array
     */
    public static function serialize($data, $groups)
    {
        $options = SerializationContext::create();
        if ($groups) {
            if (!is_array($groups)) {
                if (strpos($groups, ',') !== false) {
                    $groups = explode(',', $groups);
                } else {
                    $groups = [$groups];
                }
            }
            $options->setGroups($groups);
        }

        $serializer = SerializerBuilder::create()->build();

        return $serializer->toArray(
            $data,
            $options
        );
    }
}
