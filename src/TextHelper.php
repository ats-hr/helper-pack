<?php

namespace AtsHr\Helper;

class TextHelper
{
    public static function getter($str) {
        $str = self::underToCamel($str);
        return 'get'.ucfirst($str);
    }

    public static function setter($str) {
        $str = self::underToCamel($str);
        return 'set'.ucfirst($str);
    }

    public static function clean(&$str) {
        $str = trim(str_replace(array("\t"), '    ', $str));
        $str = trim(str_replace(array("·","","\r\n", "\r", "\n"), ' ', $str));
    }

    public static function underToCamel($str)
    {
        return \lcfirst(str_replace(' ', "", ucwords(strtr($str, '_-', ' '))));
    }

    public static function camelToUnder($camel, $trim = false)
    {
        $under = strtolower(
            preg_replace_callback(
                '/(([0-9])+|[A-Z])/',
                function ($s) {
                    return '_' . $s[1];
                },
                $camel
            )
        );
        if ($trim) {
            $under = trim($under, '_');
        }

        return $under;
    }

    public static function camelToObjectPath($camel, $numberAndCamel = true)
    {
        if ($numberAndCamel) {
            $regExp = '/(([0-9])+|[A-Z])/';
        } else {
            $regExp = '/([A-Z])/';
        }


        return strtolower(
            preg_replace_callback(
                $regExp,
                function ($s) {
                    return '.' . $s[1];
                },
                $camel
            )
        );
    }

    public static function underToObjectPath($str)
    {
        return \lcfirst(str_replace(' ', "", ucwords(strtr($str, '.', ' '))));
    }

    public static function underToNormalText($str, $ucfirst = true)
    {
        $text = str_replace("", ' ', strtr($str, '_-', ' '));
        if ($ucfirst) {
            return \ucfirst($text);
        } else {
            return $text;
        }

    }

    public static function normalToUnderText($str, $ucfirst = true)
    {
        $text = str_replace("", ' ', strtr($str, ' ', '_-'));
        if ($ucfirst) {
            return \ucfirst($text);
        } else {
            return $text;
        }

    }

    public static function slug($str, $replace = array(), $delimiter = '_', $maxLength = 2000)
    {

        if (!empty($replace)) {
            $str = str_replace((array)$replace, ' ', $str);
        }

        $replace = [
            '&lt;' => '',
            '&gt;' => '',
            '&#039;' => '',
            '&amp;' => '',
            '&quot;' => '',
            'À' => 'A',
            'Á' => 'A',
            'Â' => 'A',
            'Ã' => 'A',
            'Ä' => 'Ae',
            '&Auml;' => 'A',
            'Å' => 'A',
            'Ā' => 'A',
            'Ą' => 'A',
            'Ă' => 'A',
            'Æ' => 'Ae',
            'Ç' => 'C',
            'Ć' => 'C',
            'Č' => 'C',
            'Ĉ' => 'C',
            'Ċ' => 'C',
            'Ď' => 'D',
            'Đ' => 'D',
            'Ð' => 'D',
            'È' => 'E',
            'É' => 'E',
            'Ê' => 'E',
            'Ë' => 'E',
            'Ē' => 'E',
            'Ę' => 'E',
            'Ě' => 'E',
            'Ĕ' => 'E',
            'Ė' => 'E',
            'Ĝ' => 'G',
            'Ğ' => 'G',
            'Ġ' => 'G',
            'Ģ' => 'G',
            'Ĥ' => 'H',
            'Ħ' => 'H',
            'Ì' => 'I',
            'Í' => 'I',
            'Î' => 'I',
            'Ï' => 'I',
            'Ī' => 'I',
            'Ĩ' => 'I',
            'Ĭ' => 'I',
            'Į' => 'I',
            'İ' => 'I',
            'Ĳ' => 'IJ',
            'Ĵ' => 'J',
            'Ķ' => 'K',
            'Ł' => 'K',
            'Ľ' => 'K',
            'Ĺ' => 'K',
            'Ļ' => 'K',
            'Ŀ' => 'K',
            'Ñ' => 'N',
            'Ń' => 'N',
            'Ň' => 'N',
            'Ņ' => 'N',
            'Ŋ' => 'N',
            'Ò' => 'O',
            'Ó' => 'O',
            'Ô' => 'O',
            'Õ' => 'O',
            'Ö' => 'O',
            '&Ouml;' => 'O',
            'Ø' => 'O',
            'Ō' => 'O',
            'Ő' => 'O',
            'Ŏ' => 'O',
            'Œ' => 'OE',
            'Ŕ' => 'R',
            'Ř' => 'R',
            'Ŗ' => 'R',
            'Ś' => 'S',
            'Š' => 'S',
            'Ş' => 'S',
            'Ŝ' => 'S',
            'Ș' => 'S',
            'Ť' => 'T',
            'Ţ' => 'T',
            'Ŧ' => 'T',
            'Ț' => 'T',
            'Ù' => 'U',
            'Ú' => 'U',
            'Û' => 'U',
            'Ü' => 'U',
            'Ū' => 'U',
            '&Uuml;' => 'U',
            'Ů' => 'U',
            'Ű' => 'U',
            'Ŭ' => 'U',
            'Ũ' => 'U',
            'Ų' => 'U',
            'Ŵ' => 'W',
            'Ý' => 'Y',
            'Ŷ' => 'Y',
            'Ÿ' => 'Y',
            'Ź' => 'Z',
            'Ž' => 'Z',
            'Ż' => 'Z',
            'Þ' => 'T',
            'à' => 'a',
            'á' => 'a',
            'â' => 'a',
            'ã' => 'a',
            'ä' => 'ae',
            '&auml;' => 'ae',
            'å' => 'a',
            'ā' => 'a',
            'ą' => 'a',
            'ă' => 'a',
            'æ' => 'ae',
            'ç' => 'c',
            'ć' => 'c',
            'č' => 'c',
            'ĉ' => 'c',
            'ċ' => 'c',
            'ď' => 'd',
            'đ' => 'd',
            'ð' => 'd',
            'è' => 'e',
            'é' => 'e',
            'ê' => 'e',
            'ë' => 'e',
            'ē' => 'e',
            'ę' => 'e',
            'ě' => 'e',
            'ĕ' => 'e',
            'ė' => 'e',
            'ƒ' => 'f',
            'ĝ' => 'g',
            'ğ' => 'g',
            'ġ' => 'g',
            'ģ' => 'g',
            'ĥ' => 'h',
            'ħ' => 'h',
            'ì' => 'i',
            'í' => 'i',
            'î' => 'i',
            'ï' => 'i',
            'ī' => 'i',
            'ĩ' => 'i',
            'ĭ' => 'i',
            'į' => 'i',
            'ı' => 'i',
            'ĵ' => 'j',
            'ķ' => 'k',
            'ĸ' => 'k',
            'ł' => 'l',
            'ľ' => 'l',
            'ĺ' => 'l',
            'ļ' => 'l',
            'ŀ' => 'l',
            'ñ' => 'n',
            'ń' => 'n',
            'ň' => 'n',
            'ņ' => 'n',
            'ŉ' => 'n',
            'ŋ' => 'n',
            'ò' => 'o',
            'ó' => 'o',
            'ô' => 'o',
            'õ' => 'o',
            'ö' => 'o',
            '&ouml;' => 'o',
            'ø' => 'o',
            'ō' => 'o',
            'ő' => 'o',
            'ŏ' => 'o',
            'œ' => 'oe',
            'ŕ' => 'r',
            'ř' => 'r',
            'ŗ' => 'r',
            'š' => 's',
            'ù' => 'u',
            'ú' => 'u',
            'û' => 'u',
            'ü' => 'u',
            'ū' => 'u',
            '&uuml;' => 'u',
            'ů' => 'u',
            'ű' => 'u',
            'ŭ' => 'u',
            'ũ' => 'u',
            'ų' => 'u',
            'ŵ' => 'w',
            'ý' => 'y',
            'ÿ' => 'y',
            'ŷ' => 'y',
            'ž' => 'z',
            'ż' => 'z',
            'ź' => 'z',
            'þ' => 't',
            'ß' => 'ss',
            'ſ' => 'ss',
            'ый' => 'iy',
            'А' => 'A',
            'Б' => 'B',
            'В' => 'V',
            'Г' => 'G',
            'Д' => 'D',
            'Е' => 'E',
            'Ё' => 'YO',
            'Ж' => 'ZH',
            'З' => 'Z',
            'И' => 'I',
            'Й' => 'Y',
            'К' => 'K',
            'Л' => 'L',
            'М' => 'M',
            'Н' => 'N',
            'О' => 'O',
            'П' => 'P',
            'Р' => 'R',
            'С' => 'S',
            'Т' => 'T',
            'У' => 'U',
            'Ф' => 'F',
            'Х' => 'H',
            'Ц' => 'C',
            'Ч' => 'CH',
            'Ш' => 'SH',
            'Щ' => 'SCH',
            'Ъ' => '',
            'Ы' => 'Y',
            'Ь' => '',
            'Э' => 'E',
            'Ю' => 'YU',
            'Я' => 'YA',
            'а' => 'a',
            'б' => 'b',
            'в' => 'v',
            'г' => 'g',
            'д' => 'd',
            'е' => 'e',
            'ё' => 'yo',
            'ж' => 'zh',
            'з' => 'z',
            'и' => 'i',
            'й' => 'y',
            'к' => 'k',
            'л' => 'l',
            'м' => 'm',
            'н' => 'n',
            'о' => 'o',
            'п' => 'p',
            'р' => 'r',
            'с' => 's',
            'т' => 't',
            'у' => 'u',
            'ф' => 'f',
            'х' => 'h',
            'ц' => 'c',
            'ч' => 'ch',
            'ш' => 'sh',
            'щ' => 'sch',
            'ъ' => '',
            'ы' => 'y',
            'ь' => '',
            'э' => 'e',
            'ю' => 'yu',
            'я' => 'ya',
        ];

        $clean = str_replace(array_keys($replace), $replace, $str);
        $clean = iconv('UTF-8', 'ISO-8859-1//TRANSLIT//IGNORE', $clean);
        $clean = preg_replace("%[^-/+|\w ]%", '', $clean);
        $clean = strtolower(trim(substr($clean, 0, $maxLength), $delimiter));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

        return $clean;
    }

    public static function replaceSpecialCharacter(
        $string,
        array $specialChar = array(
            '+', /*'-',*/
            '&&',
            '||',
            '!',
            '(',
            ')',
            '{',
            '}',
            '[',
            ']',
            '^',
            '"',
            '~',
            '?',
            ':',
            '\'',
        )
    )
    {
        return str_replace($specialChar, '', $string);
    }

    public static function roman($num)
    {

        $n = intval($num);
        $result = '';
        $lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
            'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
            'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
        foreach ($lookup as $roman => $value) {
            $matches = intval($n / $value);
            $result .= str_repeat($roman, $matches);
            $n = $n % $value;
        }
        return $result;
    }


    public static function  stripAllHTMLTags($string, $remove_breaks = false)
    {
        $string = preg_replace('@<(script|style)[^>]*?>.*?</\\1>@si', '', $string);
        $string = strip_tags($string);

        if ($remove_breaks)
            $string = preg_replace('/[\r\n\t ]+/', ' ', $string);

        return trim($string);
    }

    public static function trimWords($text, $num_words = 55, $more = null)
    {
        if (null === $more) {
            $more = '&hellip;';
        }

        $original_text = $text;
        $text = self::stripAllHTMLTags($text);

        if (preg_match('/^utf\-?8$/i', 'utf-8')) {
            $text = trim(preg_replace("/[\n\r\t ]+/", ' ', $text), ' ');
            preg_match_all('/./u', $text, $words_array);
            $words_array = array_slice($words_array[0], 0, $num_words + 1);
            $sep = '';
        } else {
            $words_array = preg_split("/[\n\r\t ]+/", $text, $num_words + 1, PREG_SPLIT_NO_EMPTY);
            $sep = ' ';
        }

        if (count($words_array) > $num_words) {
            array_pop($words_array);
            $text = implode($sep, $words_array);
            $text = $text . $more;
        } else {
            $text = implode($sep, $words_array);
        }

        return $text;
    }

    /**
     * @param $content
     * @param $matches
     * @return mixed
     */
    public static function getFirstImage($content, $matches = [])
    {
        $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].+srcset=[\'"]([^\'"]+)[\'"].*>/i', $content, $matches);
        $img = @$matches[1][0];
        return $img;
    }

    public static function humanFileSize($bytes, int $decimals)
    {
        $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . ' ' . @$size[$factor];
    }

    public static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Telefon formátumának átalak0tása Probondossá
     * @param $string
     *
     * @return string
     */
    public static function telephone($string)
    {
        $string = trim($string);

        if (!$string) {
            return $string;
        }

        $string = str_replace(['/','-','(',')', ' '], "", $string);

        if (!in_array(strlen($string), [11, 12]) and strpos($string, '06') !== 0) {
            $string = '06'.$string;
        }

        if (strpos($string, '06') === 0) {
            $string = str_replace('+06', '+36', "+".$string);
        }

        $cc = substr($string, 0, 3);
        $ac = substr($string, 3, 2);
        if ($ac < 20) {
            $ac = 1;
            $p1 = substr($string, 4, 3);
            $p2 = substr($string, 7, 4);
        } elseif (!in_array($ac, [20,30,50,70])) {
            $p1 = substr($string, 5, 3);
            $p2 = substr($string, 8, 3);
        } else {
            $p1 = substr($string, 5, 3);
            $p2 = substr($string, 8, 4);
        }

        return "$cc $ac $p1 $p2";
    }

    /**
     * @param $hash
     * @return bool
     */
    public static function isMongoHash($hash): bool
    {
        return self::isHash($hash, 24);
    }

    /**
     * @param $hash
     * @param int $length
     * @return bool
     */
    public static function isHash($hash, $length = 32): bool
    {
        return preg_match("/[a-f0-9]{".$length."}/i", $hash) ? true : false;
    }
}
