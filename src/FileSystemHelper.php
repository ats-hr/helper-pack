<?php

namespace AtsHr\Helper;

use Symfony\Component\Filesystem\Filesystem;

/**
 * Class FileSystemHelper
 *
 * @package Probond\CRMBundle\Component\Helper
 */
class FileSystemHelper
{
    /**
     * @param $dir
     *
     * @return string
     */
    public static function createFolder($dir)
    {
        $fs = new Filesystem();
        if (!is_dir($dir)) {
            $fs->mkdir($dir, 0777);
        }

        return $dir;
    }

    /**
     * @param $path
     * @param string $separator
     * @return string
     */
    public static function getFilenameFromFullPath($path, $separator = DIRECTORY_SEPARATOR)
    {
        $array = explode($separator, $path);
        return array_pop($array);
    }

    /**
     * @param $url
     * @param $dir
     * @param null $filename
     * @return \SplFileInfo
     */
    public static function download($url, $dir, $filename = null)
    {
        $content = file_get_contents($url);

        if (!$filename){
            $urlArray = parse_url($url);
            $urlPath = $urlArray['path'];
            $filename = self::getFilenameFromFullPath($urlPath, '/');
        }
        $path = self::createFolder($dir).'/'.$filename;
        file_put_contents($path, $content);

        return new \SplFileInfo($path);
    }
}
