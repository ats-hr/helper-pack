<?php

namespace AtsHr\Helper;

class MapHelper
{
    /**
     * Get a center latitude,longitude from an array of like geopoints
     *
     * @param array data 2 dimensional array of latitudes and longitudes
     * For Example:
     * $data = array
     * (
     *   0 = > array(45.849382, 76.322333),
     *   1 = > array(45.843543, 75.324143),
     *   2 = > array(45.765744, 76.543223),
     *   3 = > array(45.784234, 74.542335)
     * );
     * @return array|bool
     */
    public static function center($data)
    {
        if (!is_array($data)) return FALSE;

        $num_coords = count($data);

        $X = 0.0;
        $Y = 0.0;
        $Z = 0.0;

        foreach ($data as $coord)
        {
            $lat = $coord[0] * pi() / 180;
            $lon = $coord[1] * pi() / 180;

            $a = cos($lat) * cos($lon);
            $b = cos($lat) * sin($lon);
            $c = sin($lat);

            $X += $a;
            $Y += $b;
            $Z += $c;
        }

        $X /= $num_coords;
        $Y /= $num_coords;
        $Z /= $num_coords;

        $lon = atan2($Y, $X);
        $hyp = sqrt($X * $X + $Y * $Y);
        $lat = atan2($Z, $hyp);

        return array('latitude' => $lat * 180 / pi(), 'longitude' => $lon * 180 / pi());
    }

    // mean radius of the earth (miles) at 39 degrees from the equator
    const MILE = 3961;

    // mean radius of the earth (km) at 39 degrees from the equator
    const KILOMETER = 6373;

    public static function distance($lat1, $lon1, $lat2, $lon2, $unit = 'km') {
        // convert coordinates to radians
        $lat1 = deg2rad($lat1);
        $lon1 = deg2rad($lon1);
        $lat2 = deg2rad($lat2);
        $lon2 = deg2rad($lon2);

        // find the differences between the coordinates
        $diffLat = $lat2 - $lat1;
        $diffLon= $lon2 - $lon1;

        // here's the heavy lifting
        $a  = (double)pow(sin($diffLat/2),2) + cos($lat1) * cos($lat2) * pow(sin($diffLon/2),2);
        $circleDistanceRadian  = (double)2 * atan2(sqrt($a),sqrt(1-$a)); // great circle distance in radians

        switch ($unit) {
            case "km":
                return round( ($circleDistanceRadian * self::KILOMETER) * 1000) / 1000;
                break;
            case "mile":
                return round( ($circleDistanceRadian * self::MILE) * 1000) / 1000;
                break;
            default:
                return number_format($circleDistanceRadian, 12);
        }
    }

    public static function km2rad(float $km) {
        return number_format($km / 6378 * 1000, 8);
    }

    public static function miles2rad(float $km) {
        return number_format($km / 3959 * 1000, 8);
    }
}
