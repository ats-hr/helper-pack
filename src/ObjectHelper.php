<?php

namespace AtsHr\Helper;

class ObjectHelper
{
    /**
     * @param $object
     * @param string|\ReflectionProperty $path
     * @param mixed $default
     *
     * @return mixed
     * @throws \ReflectionException
     */
    public static function access($object, $path, $default = '')
    {
        try {
            if ($path instanceof \ReflectionProperty) {
                $path->setAccessible(\ReflectionProperty::IS_PUBLIC);
                return $path->getValue($object);
            }

            if (is_object($object)) {
                $refl = new \ReflectionObject($object);
                $paths = explode('.', $path);

                foreach ($paths as $key => $pathItem) {
                    unset($paths[$key]);
                    $property = $refl->getProperty($pathItem);
                    $property->setAccessible(\ReflectionProperty::IS_PUBLIC);
                    $value = $property->getValue($object);
                    if (is_array($value)) {
                        return ArrayHelper::getValueByPath($value, join('.', $paths), $default);
                    } elseif (is_object($value)) {
                        return self::access($value, join('.', $paths), $default);
                    } else {
                        return $value ?? $default;
                    }
                }
            } elseif (is_array($object)) {
                return ArrayHelper::getValueByPath($object, $path, $default);
            } else {
                return $object ?? $default;
            }

            return $default;
        } catch (\Exception $e) {
            return $default;
        }
    }
}
