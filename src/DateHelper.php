<?php

namespace AtsHr\Helper;

use DateInterval;
use DateTime;
use Exception;

class DateHelper
{
    public static function create($date = null)
    {
        return new \DateTime($date);
    }

    /**
     * @param DateTime $date
     * @return DateTime
     * @throws Exception
     */
    public static function tomorrow($date)
    {
        return $date->add(new DateInterval('P1D'));
    }

    /**
     * @param DateTime $date
     * @return DateTime
     * @throws Exception
     */
    public static function yesterday($date)
    {
        return $date->sub(new DateInterval('P1D'));
    }

    public static function days()
    {
        return ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
    }

    /**
     * @return string
     */
    public static function atNextQuarter()
    {
        return self::prepareNextDate(15);
    }

    /**
     * @return string
     */
    public static function atNext5thMin()
    {
        return self::prepareNextDate(5);
    }

    /**
     * @param array $options ['start','end']
     *
     * @return array
     */
    public static function getYearsArray($options = [])
    {
        if (!isset($options['start'])) {
            $options['start'] = 1950;
        }
        if (!isset($options['end'])) {
            $options['end'] = date('Y');
        }

        $years = array();
        foreach (range($options['start'], $options['end']) as $y) {
            $years[$y] = $y;
        }

        return $years;
    }

    /**
     * @param $date
     *
     * @return bool
     */
    public static function isDate($date)
    {
        return (bool)preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $date);
    }

    /**
     * @param $date
     *
     * @return bool
     */
    public static function isYearWeek($date)
    {
        list($year, $week) = explode('-', $date);
        if (
            preg_match('/^[0-9]{4}/', $year)
            and
            $week < 54
        ) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * param PhpOffice\PhpSpreadsheet\Cell\Cell $cell
     * @param $cell
     * @param bool $dateTimeObject
     * @return string
     */
    public static function excelDate($cell, $dateTimeObject = false)
    {
        if (!class_exists('PhpOffice\PhpSpreadsheet\Cell\Cell')) {
            @trigger_error("Missing phpspreadsheets component!");
            return '';
        }

//        $formatCode = $cell->getWorksheet()->getParent()->getCellXfByIndex($cell->getXfIndex())
//            ->getNumberFormat()->getFormatCode();

        if (self::isDate($cell->getValue())) {
            return $cell->getValue();
        }

        if (\PhpOffice\PhpSpreadsheet\Shared\Date::isDateTime($cell)) {
            $date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($cell->getValue());
            if ($dateTimeObject) {
                return $date;
            }

            $date = $date->format('Y-m-d');

        } else {
            $date = trim($cell->getValue());
        }

        return $date;
    }

    /**
     * get date in the next x. minute
     * ex: the next 15th minute (00, 15, 30, 45)
     *
     * @param int $m minutes
     *
     * @return string
     */
    protected static function prepareNextDate($m)
    {
        $t = date('i');

        $min = $t + ($m - $t % $m);

        str_pad($min, 2, 0, STR_PAD_LEFT);
        if ($min >= 60) {
            $min = '00';
            $date = date('Y-m-d H:', time() + 60 * 60) . $min;
        } else {
            $date = date('Y-m-d H:') . $min;
        }

        return "$date:00";
    }


    /**
     * affected weeks between parameters
     *
     * @param DateTime $start
     * @param DateTime $end
     *
     * @return array
     * @throws Exception
     */
    public static function affectedWeeks($start, $end)
    {
        $weeks = [];
        while ($start <= $end) {
            $weeks[] = $start->format('Y-W');
            $start->add(new DateInterval('P5D'));
        }

        return array_unique($weeks);
    }

    /**
     * @param null $year
     * @param null $week
     *
     * @return DateTime|false
     * @throws Exception
     */
    public static function firstDayOfWeek($year = null, $week = null)
    {
        return (new DateTime())
            ->setISODate($year ?? date("Y"), $week ?? date("W"), 1)
            ->setTime(0, 0, 0);
    }

    /**
     * @param null $year
     * @param null $week
     *
     * @return DateTime|false
     * @throws Exception
     */
    public static function lastDayOfWeek($year = null, $week = null)
    {
        return (new DateTime())
            ->setISODate($year ?? date("Y"), $week ?? date("W"), 7)
            ->setTime(23, 59, 59);
    }


    /**
     * get affected months between dates
     *
     * @param DateTime $start
     * @param DateTime $end
     *
     * @return array
     * @throws Exception
     */
    public static function affectedMonth($start, $end)
    {
        $months = [];
        while ($start <= $end) {
            $months[] = $start->format('Y-m');
            $start->add(new DateInterval('P5D'));
        }

        return array_unique($months);
    }

    /**
     * @param null $year
     * @param null $month
     *
     * @return DateTime|false
     * @throws Exception
     */
    public static function firstDayOfMonth($year = null, $month = null)
    {
        $year = $year ?? date("Y");
        $month = $month ?? date("m");
        return (new DateTime("$year-$month-12"))
            ->modify("first day of this month")
            ->setTime(0, 0, 0);
    }

    /**
     * @param null $year
     * @param null $month
     *
     * @return DateTime|false
     * @throws Exception
     */
    public static function lastDayOfMonth($year = null, $month = null)
    {
        $year = $year ?? date("Y");
        $month = $month ?? date("m");
        return (new DateTime("$year-$month-12"))
            ->modify("last day of this month")
            ->setTime(23, 59, 59);
    }


    /**
     * get affected months between dates
     *
     * @param DateTime $start
     * @param DateTime $end
     *
     * @return array
     * @throws Exception
     */
    public static function affectedYears($start, $end)
    {
        $years = [];
        while ($start <= $end) {
            $years[] = $start->format('Y');
            $start->add(new DateInterval('P5D'));
        }

        return array_unique($years);
    }

    /**
     * @param null $year
     *
     * @return DateTime|false
     * @throws Exception
     */
    public static function firstDayOfYear($year = null)
    {
        return (new DateTime("$year-01-01"))
            ->setTime(0, 0, 0);
    }

    /**
     * @param null $year
     *
     * @return DateTime|false
     * @throws Exception
     */
    public static function lastDayOfYear($year = null)
    {
        return (new DateTime("$year-12-31"))
            ->setTime(23, 59, 59);
    }

    /**
     * @return array
     * @throws Exception
     */
    public static function lastWeek()
    {
        return [
            'start' => new DateTime('last week monday'),
            'end' => new DateTime('last week sunday'),
        ];
    }

    /**
     * @return array
     * @throws Exception
     */
    public static function lastTwoWeek()
    {
        $lastWeek = date("Y-m-d", strtotime("last week monday"));
        $weekbeforeLast = new DateTime($lastWeek . "-1week");

        $end = date("Y-m-d", strtotime("last week sunday"));

        return [
            'start' => $weekbeforeLast,
            'end' => new DateTime($end)
        ];
    }

    /**
     * @return array
     * @throws Exception
     */
    public static function lastMonth()
    {
        $ym = date('Y-m', strtotime("last month"));
        list($year, $month) = explode('-', $ym);
        return [
            'start' => self::firstDayOfMonth($year, $month),
            'end' => self::lastDayOfMonth($year, $month)
        ];
    }

    /**
     * @return array
     * @throws Exception
     */
    public static function lastHalfYear()
    {
        return [
            'start' => new DateTime("-6 months"),
            'end' => new DateTime()
        ];
    }

    /**
     * @return array
     * @throws Exception
     */
    public static function lastYear()
    {
        $year = date('Y', strtotime('last year'));
        return [
            'start' => self::firstDayOfYear($year),
            'end' => self::lastDayOfYear($year)
        ];
    }


    /**
     * @return array
     * @throws Exception
     */
    public static function thisYear()
    {
        $year = date('Y');
        return [
            'start' => self::firstDayOfYear($year),
            'end' => self::lastDayOfYear($year)
        ];
    }

    /**
     * @param integer $quarter
     * @param null $year
     * @return array
     * @throws Exception
     */
    public static function getQuarterByNum($quarter, $year = null)
    {
        if (!$year) {
            $year = date('Y');
        }

        $quarters = [
            1 => [
                'start' => "{$year}-01-01",
                'end' => "{$year}-03-31"
            ],
            2 => [
                'start' => "{$year}-04-01",
                'end' => "{$year}-06-30"
            ],
            3 => [
                'start' => "{$year}-07-01",
                'end' => "{$year}-09-30"
            ],
            4 => [
                'start' => "{$year}-10-01",
                'end' => "{$year}-12-31"
            ]
        ];

        return [
            'start' => new DateTime($quarters[$quarter]['start']),
            'end' => new DateTime($quarters[$quarter]['end'])
        ];
    }

    /**
     * @param array $date
     * @return array
     * @throws Exception
     */
    public static function parseDateInterval(array $date): array
    {
        $interval = null;
        $year = date('Y');
        if (isset($date['year'])) {
            $year = $date['year'];
        }
        if (
            (array_key_exists('start', $date) && $date['start'])
            && (array_key_exists('end', $date) && $date['end'])
        ) {
            $interval['start'] = new DateTime($date['start']);
            $interval['end'] = new DateTime($date['end']);
        } else {
            switch ($date['selected']) {
                case 'last_week':
                    $interval = self::lastWeek();
                    break;
                case 'last_two_week':
                    $interval = self::lastTwoWeek();
                    break;
                case 'last_month':
                    $interval = self::lastMonth();
                    break;
                case 'last_half_year':
                    $interval = self::lastHalfYear();
                    break;
                case 'last_year':
                    $interval = self::lastYear();
                    break;
                case 'actual_year':
                    $interval = self::thisYear();
                    break;
                case 'actual_month':
                    $interval = ['start' => self::firstDayOfMonth(), 'end' => self::lastDayOfMonth()];
                    break;
                case 'q1':
                    $interval = self::getQuarterByNum(1, $year);
                    break;
                case 'q2':
                    $interval = self::getQuarterByNum(2, $year);
                    break;
                case 'q3':
                    $interval = self::getQuarterByNum(3, $year);
                    break;
                case 'q4':
                    $interval = self::getQuarterByNum(4, $year);
                    break;
            }
        }

        return $interval;
    }
}
