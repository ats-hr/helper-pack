<?php
/**
 * Created by JetBrains PhpStorm.
 * User: roberto
 * Date: 2013.10.27.
 * Time: 14:12
 * To change this template use File | Settings | File Templates.
 */

namespace AtsHr\Helper;

use AtsHr\Helper\TextHelper;

class ArrayHelper
{

    /**
     * Sets a value in a nested array based on path
     *
     * @param array $array The array to modify
     * @param string $path The path in the array
     * @param mixed $value The value to set
     * @param string $delimiter The separator for the path
     * @return array The previous value
     */
    public static function setNestedArrayValue(&$array, $path, &$value, $delimiter = '.')
    {
        $pathParts = explode($delimiter, $path);

        $current = &$array;
        foreach ($pathParts as $key) {
            $current = &$current[$key];
        }

        $backup = $current;
        $current = $value;

        return $backup;
    }

    /**
     * @param $a
     * @return array|bool
     */
    public static function camelToUnderscoreAssocArrayRecursive($a)
    {
        if (is_array($a)) {
            $b = array();
            array_walk_recursive(
                $a,
                function ($item, $key) use ($a, &$b) {
                    $newKey = TextHelper::camelToUnder($key);
                    $b[$newKey] = $item;
                }
            );

            return $b;
        }

        return false;
    }

    public static function underscoreToCamelAssocArrayRecursive($a)
    {
        if (is_array($a)) {
            $b = array();
            array_walk_recursive(
                $a,
                function ($item, $key) use ($a, &$b) {
                    $newKey = TextHelper::underToCamel($key);
                    $b[$newKey] = $item;
                }
            );

            return $b;
        }

        return false;
    }

    /**
     * @param $a array
     * @param $remove string || array
     */
    public static function arrayUnsetRecursive(array &$a, $remove)
    {
        $arraySort = function ($value) {

            $assocTemp = array();

            foreach ($value as $index => $item) {
                if (is_string($index)) {
                    $assocTemp[$index] = is_string($item)?trim($item):$item;
                    unset($value[$index]);
                }
            }

            $value = array_values($value);

            if (!empty($assocTemp)) {
                $value = array_merge($value, $assocTemp);
            }

            return $value;
        };

        if (!is_array($remove)) {
            $remove = array($remove);
        }
        foreach ($a as $key => &$value) {
            if (is_array($value)) {
                $value = $arraySort($value);
                self::arrayUnsetRecursive($value, $remove);
            }
            if (in_array(is_string($value)?trim($value):$value, $remove)) {
                unset($a[$key]);
                $a = $arraySort($a);
            }
        }

    }


    public static function merge($a, $b)
    {
        if (!$b) {
            return $a;
        } elseif (is_array($b) && is_array($a) && (self::isAssociative($b) || empty($b) || self::isAssociative($a))) {
            foreach ($b as $key => $value) {
                if (array_key_exists($key, $a)) {
                    if (!$value) {
                        $a[$key] = self::merge($a[$key], $value);
                    }
                } else {
                    if (!$value) {
                        $a[$key] = $value;
                    }
                }
            }
        } else {
            $a = $b;
        }

        return $a;
    }

    public static function isAssociative(array $arr)
    {
        $keys = array_keys($arr);

        return empty($arr) ? false : $keys !== array_keys($keys);
    }

    private static function removeNullObjectsFromArray(array $a)
    {
        foreach ($a as $key => $value) {
            if (isset($a[$key]) && $a[$key] === null) {
                unset($a[$key]);
            }
        }

        return $a;
    }

    /**
     * @param array $a
     * @param array $b
     *
     * @return array
     */
    public static function diff($a, $b)
    {
        //preprocess NullObject values

        if (is_array($a)) {
            $a = self::removeNullObjectsFromArray($a);
        }

        if (is_array($b)) {
            $b = self::removeNullObjectsFromArray($b);
        } elseif ($b === null) {
            return $a;
        }

        $difference = array();
        foreach ($a as $key => $value) {
            if (is_array($value)) {
                if (!isset($b[$key]) || !is_array($b[$key])) {
                    $difference[$key] = $value;
                } else {
                    $subDiff = self::diff($value, $b[$key]);
                    if (!empty($subDiff)) {
                        $difference[$key] = $subDiff;
                    }
                }
            } elseif (!array_key_exists($key, $b) || $b[$key] !== $value) {
                //This makes this way of subtraction different from array_diff
                if (self::isAssociative($b)) {
                    $difference[$key] = $value;
                } else {
                    $difference[] = $value;
                }
            }
        }

        return $difference;
    }

    public static function getValueByPath($data, $pathString, $default = null)
    {
        $path = explode('.', $pathString);
        $tmpData = $data;
        while (!empty($tmpData) && !empty($path)) {
            $key = array_shift($path);
            if (array_key_exists($key, $tmpData)) {
                $tmpData = $tmpData[$key];
            } else {
                return $default;
            }
        }

        return $tmpData;
    }


    /**
     * @param $data
     * @param $pathString
     * @param $value
     * @return mixed
     */
    public static function setValueByPath($data, $pathString, $value)
    {
        $path = explode('.', $pathString);
        $tmpData = &$data;
        while (count($path) > 1) {
            $key = array_shift($path);

            if (!array_key_exists($key, $tmpData)) {
                $tmpData[$key] = array();
            }
            $tmpData = &$tmpData[$key];
        }

        $tmpData[array_shift($path)] = $value;

        return $data;
    }

    public static function prefixKeys($data, $prefix)
    {
        $result = array();
        foreach ($data as $key => $value) {
            $result[$prefix.$key] = $value;
        }

        return $result;
    }

    public static function maxRecursive(array $data)
    {
        $max = 0;
        foreach ($data as $value) {
            $candidate = is_array($value) ? self::maxRecursive($value) : $value;
            $max = $candidate > $max ? $candidate : $max;
        }

        return $max;
    }

    public static function findHighestValueInMultiArray($data, $indexName)
    {
        return array_reduce(
            $data,
            function ($a, $b) use ($indexName) {
                return @$a[$indexName] > $b[$indexName] ? $a : $b;
            }
        );
    }

    public static function subtractMatrices(array $a, array $b)
    {
        for ($i = 0, $c = count($b); $i < $c; $i++) {
            $a[$i] = is_array($a[$i])
                ? self::subtractMatrices($a[$i], $b[$i])
                : $a[$i] - $b[$i];
        }

        return $a;
    }

    public static function calculateChangeRateMatrices(array $a, array $b)
    {
        if (count($a) != count($b)) {
            throw new \InvalidArgumentException('Size of arrays differs');
        }

        for ($i = 0, $c = count($b); $i < $c; $i++) {
            $a[$i] = is_array($a[$i])
                ? self::calculateChangeRateMatrices($a[$i], $b[$i])
                : (
                $a[$i] != 0
                    ? (($b[$i] - $a[$i]) / $a[$i])
                    : (($b[$i] - $a[$i]) > 0 ? 1 : -1)
                );
        }

        return $a;
    }

    public static function mapRecursive(array $data, $callable)
    {
        foreach ($data as $key => &$item) {
            if (is_array($item)) {
                $item = self::mapRecursive($item, $callable);
            } else {
                $item = call_user_func($callable, $item, $key);
            }
        }

        return $data;
    }


    public static function recursive_array_search($needle, $haystack)
    {
        foreach ($haystack as $key => $value) {
            $current_key = $key;
            if ($needle === $value or (is_array($value) && self::recursive_array_search($needle, $value) !== false)) {
                return $current_key;
            }
        }

        return false;
    }
    
    public static function recursiveFind(array $array, $needle)
    {
        $iterator  = new \RecursiveArrayIterator($array);
        $recursive = new \RecursiveIteratorIterator($iterator, \RecursiveIteratorIterator::SELF_FIRST);
        $aHitList = array();
        foreach ($recursive as $key => $value) {
            if ($key === $needle) {
                array_push($aHitList, $value);
            }
        }
        return $aHitList;
    }


    public static function unique(array $array, $indexed = true, $callback = null)
    {
        $result = array();
        foreach ($array as $item) {
            if ($callback) {
                $value = call_user_func($callback, $item);
            } else {
                $value = TextHelper::slug($item);
            }

            $result[$value] = $item;
        }

        if ($indexed == true) {
            return $result;
        } else {
            return array_values($result);
        }
    }

    public static function findByKey($array, $key, $needle) {
        foreach ($array as $k => $v) {
            $v = (array)$v;
            if(array_search($needle, $v)) {
                return $k;
            }
        }

        return false;
    }

    public static function findByPath($array, $path, $needle)
    {
        foreach ($array as $item) {
            if (self::getValueByPath($item, $path) == $needle) {
                return true;
            }
        }

        return false;
    }
    public static function findValueByPath($data, $pathString, $default = null)
    {
        $paths = explode('.', $pathString);

        foreach ((array) $paths as $path) {
            if (array_key_exists($path, $data)) {
                $data = &$data[$path];
            } else {
                return $default;
            }
        }

        return $data;
    }

    public static function removeNullItems(&$array){
        foreach ($array as $k => $v) {
            if (is_array($v) and empty($v)) {
                unset($array[$k]);
            } elseif (empty($v) && strlen($v) == 0) {
                unset($array[$k]);
            }
        }
    }

    public static function valueAsKey($array) {
        $result = array();
        foreach ($array as $item) {
            $result[$item] = $item;
        }

        return $result;
    }

    /**
     * @param array $ids
     *
     * @return array
     */
    public static function idTomongoId($ids)
    {
        return array_map(function ($item) {
            return \MongoId::isValid($item) ? new \MongoId($item) : $item;
        }, $ids);
    }

    /**
     * @param array $sample
     * @param array $arrayToOrder
     * @return array
     */
    public static function orderAssociativeArray(array $sample, array $arrayToOrder): array
    {
        $ordered = array_replace(array_flip($sample), $arrayToOrder);
        return $ordered;
    }

    /**
     * @param array $sample
     * @param array $arrayToOrder
     * @param string $paramName
     * @return array
     */
    public static function orderArray(array $sample, array $arrayToOrder, string $paramName = 'id'): array
    {
        $getter = TextHelper::getter($paramName);
        $ordered = array_flip($sample);

        foreach ($arrayToOrder as $o) {
            if (method_exists($o, $getter) && in_array($o->$getter(), $sample)) {
                $ordered[$o->$getter()] = $o;
            }
        }

        return array_values($ordered);
    }

    /**
     * @param array $main
     * @param array $toCompare
     * @param array $filters
     * @return array
     */
    public static function arrayIntersectFilter(array $main, array $toCompare, array $filters): array
    {
        $matchingKeys = [];
        foreach ($filters as $key) {
            if (
                (array_key_exists($key, $main) && array_key_exists($key, $toCompare))
                && $main[$key] == $toCompare[$key]
            ) {
                $matchingKeys[] = $key;
            }
        }

        return $matchingKeys;
    }

    /**
     * ARRAY FUNCTIONS FOR array|\Iterator
     */

    /**
     * @param array|\Iterator $array
     * @return int
     */
    public static function count($array)
    {
        if (is_object($array) and $array instanceof \Iterator){
            return iterator_count($arary);
        }

        return count($array);
    }
    

    /**
     * @param $needle
     * @param array|\Iterator $array
     * @return bool
     */
    public static function inArray($needle, $array)
    {
        if (is_object($array) and $array instanceof \Iterator){
            $array = iterator_to_array($array);
        }

        return in_array($needle , $array);
    }
}
